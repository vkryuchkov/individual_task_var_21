#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>
#include <cstdio>
#include <vector>
#include <sstream>
#include <fstream>
#include <ctime>

using namespace std;

typedef struct Service {
	string name;
	unsigned int code;
	float tariff;
	string time_span;
};

typedef struct Clients {
	string fio;
	string number;
	string date_conclusion_contract;
	string date_expired_contract;
};

typedef struct Stats {
	string number;
	unsigned int code;
	string date_of_use;
	string time_of_use;
	unsigned int time;
};

vector<Service> services;
vector<Stats> stats;
vector<Clients> clients;

vector<string> split(string str, string del) {
	vector<string> sv;
	stringstream ss(str);
	string item;
	while (getline(ss, item, del[0])) {
		sv.push_back(item);
	}
	return sv;
}

void getClients() {
	string l;
	ifstream ifs("clients.txt");
	vector<string> strs;
	Clients cl;
	while (!ifs.eof()) {
		getline(ifs, l);
		strs = split(l, ",");
		cl.fio = strs[0];
		cl.number = strs[1];
		cl.date_conclusion_contract = strs[2];
		cl.date_expired_contract = strs[3];
		clients.push_back(cl);
	}
}

void getStats() {
	string l;
	ifstream ifs("stats.txt");
	vector<string> subs;
	vector<string> date;
	Stats st;
	while (!ifs.eof()) {
		getline(ifs, l);
		subs = split(l, ",");
		st.number = subs[0];
		st.code = atoi(subs[1].c_str());
		date = split(subs[2], " ");
		st.date_of_use = date[0];
		st.time_of_use = date[1];
		st.time = atoi(subs[3].c_str());
		stats.push_back(st);
	}
}

void getServices() {
	string lin;
	ifstream ifs("serv.txt");
	vector<string> subs;
	Service s;
	while (!ifs.eof()) {
		getline(ifs, lin);
		subs = split(lin, ",");
		s.name = subs[0];
		s.code = stoi(subs[1].c_str());
		s.tariff = atof(subs[2].c_str());
		s.time_span = subs[3];
		services.push_back(s);
	}
}

vector<string> getParams() {
	string l;
	ifstream ifs("Param.ini");
	vector<string> params;
	getline(ifs, l);
	params = split(l, ",");
	return params;
}

int getMonth(string date)
{
	vector<string> v = split(date, ".");
	return stoi(v[1].c_str());
}

int getYear(string date)
{
	vector<string> v = split(date, ".");
	return stoi(v[2].c_str());
}

Service findService(string name) {
	for (int i = 0; i < services.size(); i++) {
		if (services[i].name == name)
			return services[i];
	}
}

Stats findStat(int code) {
	for (int i = 0; i < stats.size(); i++) {
		if (stats[i].code == code)
			return stats[i];
	}
}

int main() {
	setlocale(LC_ALL, "Russian");
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	int current_year = timeinfo->tm_year + 1900;
	int current_month = timeinfo->tm_mon + 1;
	int current_day = timeinfo->tm_mday;
	getClients();
	getStats();
	getServices();
	getParams();
	float costs = 0;
	float summ = 0;
	for (int i = 0; i < getParams().size(); i++) {
		int code = findService(getParams()[i]).code;
		float tariff = findService(getParams()[i]).tariff;
		string time_span = findService(getParams()[i]).time_span;
		string date = findStat(code).date_of_use;
			if ((getYear(date) == current_year) && (getMonth(date) >= (current_month - 3))){
				if (time_span == "���")
					costs = tariff * (findStat(code).time / 60);
				else if (time_span == "�����")
					costs = tariff * (findStat(code).time / 86400);
				else if (time_span == "�����")
					costs = tariff * (findStat(code).time / 2592000);
				else if (time_span == "#")
					costs = tariff;
				summ += costs;
			}
	}
	ofstream ofs("Report.txt");
	ofs << summ << endl;
	system("pause");
	return 0;
}